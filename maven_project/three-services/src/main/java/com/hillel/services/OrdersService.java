package com.hillel.services;

import com.hillel.dao.OrdersRepository;
import com.hillel.entities.OrderEntity;
import com.hillel.services.interfaces.IDomainService;

public class OrdersService implements IDomainService<OrderEntity> {
    OrdersRepository repository;

    public OrdersService(OrdersRepository repository) {
        this.repository = repository;
    }

    @Override
    public OrderEntity get() {
        return repository.find();
    }
}
