package com.hillel.dao.interfaces;

public interface IRepository<T> {
    T find();
}
