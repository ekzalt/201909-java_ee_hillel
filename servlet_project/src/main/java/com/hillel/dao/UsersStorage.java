package com.hillel.dao;

import com.hillel.enums.LoggedStatus;
import com.hillel.enities.User;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.Data;

@Data
public class UsersStorage {
    private List<User> users = new ArrayList<>(Arrays.asList(
            new User("First1", "Last1", "username1", "111", LoggedStatus.NOT_LOGGED),
            new User("First2", "Last2", "username2", "222", LoggedStatus.NOT_LOGGED),
            new User("First3", "Last3", "username3", "333", LoggedStatus.NOT_LOGGED)
    ));

    public User findFirstMatch(String firstName, String lastName, String username) {
        return users
                .stream()
                .filter(user -> {
                    if (user.getFirstName().equals(firstName)) return true;
                    if (user.getLastName().equals(lastName)) return true;
                    if (user.getUsername().equals(username)) return true;

                    return false;
                })
                .findFirst()
                .orElse(null);
    }

    public User findFullMatch(String firstName, String lastName, String username, String password) {
        return users
                .stream()
                .filter(user -> {
                    if(user.getFirstName().equals(firstName) && user.getLastName().equals(lastName) && user.getUsername().equals(username) && user.getPassword().equals(password)) return true;

                    return false;
                })
                .findFirst()
                .orElse(null);
    }

    public void save(User user) {
        users.add(user);
    }

    public void update(User user, String firstName, String lastName, String username) {
        if (firstName != null) user.setFirstName(firstName);
        if (lastName != null) user.setFirstName(lastName);
        if (username != null) user.setFirstName(username);
    }

    public void delete(User user) {
        users.remove(user);
    }
}
