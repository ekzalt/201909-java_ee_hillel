package com.hillel.listeners;

import com.hillel.dao.UsersStorage;
import com.hillel.services.UsersService;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext context = servletContextEvent.getServletContext();
        UsersStorage storage = new UsersStorage();
        UsersService service = new UsersService(storage);

        context.setAttribute("usersStorage", storage);
        context.setAttribute("usersService", service);

        System.out.println(context.getAttributeNames());
        System.out.println(context.getInitParameterNames());
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}
