package com.hillel.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggerService {
    public static final Logger logger = LogManager.getLogger("LoggerService");
}
