package com.hillel.services;

import com.hillel.dao.UsersStorage;
import com.hillel.enums.LoggedStatus;
import com.hillel.enities.User;
import lombok.Data;

import java.util.List;

@Data
public class UsersService {
    private UsersStorage storage;
    public static UsersService service = new UsersService(new UsersStorage());

    public UsersService(UsersStorage storage) {
        this.storage = storage;
    }

    public List<User> findAdd() {
        return storage.getUsers();
    }

    public User findFirstMatch(String firstName, String lastName, String username) {
        return storage.findFirstMatch(firstName, lastName, username);
    }

    public User findFullMatch(String firstName, String lastName, String username, String password) {
        return storage.findFullMatch(firstName, lastName, username, password);
    }

    public void save(String firstName, String lastName, String username, String password) {
        storage.save(new User(firstName, lastName, username, password, LoggedStatus.NOT_LOGGED));
    }

    public void update(User user, String firstName, String lastName, String username) {
        storage.update(user, firstName, lastName, username);
    }

    public void delete(User user) {
        storage.delete(user);
    }

    public void login(User user) {
        user.setStatus(LoggedStatus.LOGGED);
    }
}
