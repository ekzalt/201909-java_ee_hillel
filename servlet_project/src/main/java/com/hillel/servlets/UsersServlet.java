package com.hillel.servlets;

import com.hillel.services.LoggerService;
import com.hillel.services.UsersService;
import com.hillel.services.JsonService;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/users")
public class UsersServlet extends HttpServlet {
    public static final String CHARSET = "UTF-8";
    private ServletContext context;

    @Override
    public void init() {
        context = getServletContext();

        context.setAttribute("logger", LoggerService.logger);
        context.setAttribute("usersService", UsersService.service);
        context.setAttribute("objectMapper", JsonService.mapper);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Logger logger = (Logger) context.getAttribute("logger");
        UsersService service = (UsersService) context.getAttribute("usersService");
        ObjectMapper mapper = (ObjectMapper) context.getAttribute("objectMapper");

        logger.info("GET /users");

        response.setCharacterEncoding(CHARSET);
        response.setContentType("application/json");
        PrintWriter pw = response.getWriter();
        pw.println(mapper.writeValueAsString(service.findAdd()));
        pw.close();
    }
}
