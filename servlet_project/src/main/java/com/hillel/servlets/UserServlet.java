package com.hillel.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hillel.enities.User;
import com.hillel.services.JsonService;
import com.hillel.services.LoggerService;
import com.hillel.services.UsersService;

import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/user")
public class UserServlet extends HttpServlet {
    public static final String CHARSET = "UTF-8";
    private ServletContext context;

    @Override
    public void init() {
        context = getServletContext();

        context.setAttribute("logger", LoggerService.logger);
        context.setAttribute("usersService", UsersService.service);
        context.setAttribute("objectMapper", JsonService.mapper);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Logger logger = (Logger) context.getAttribute("logger");
        UsersService service = (UsersService) context.getAttribute("usersService");
        ObjectMapper mapper = (ObjectMapper) context.getAttribute("objectMapper");

        logger.info("GET /user");
        String username = request.getParameter("username");

        response.setCharacterEncoding(CHARSET);
        response.setContentType("application/json");

        if (username == null || username.isEmpty()) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        User user = service.findFirstMatch(null, null, username);

        if (user == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        PrintWriter pw = response.getWriter();
        pw.println(mapper.writeValueAsString(user));
        pw.close();
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Logger logger = (Logger) context.getAttribute("logger");
        UsersService service = (UsersService) context.getAttribute("usersService");
        ObjectMapper mapper = (ObjectMapper) context.getAttribute("objectMapper");

        logger.info("POST /user");

        String firstName = null;
        String lastName = null;
        String username = null;
        String password = null;

        try {
            User mapped = mapper.readValue(request.getReader(), User.class);
            firstName = mapped.getFirstName();
            lastName = mapped.getLastName();
            username = mapped.getUsername();
            password = mapped.getPassword();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        if (firstName == null || lastName == null || username == null || password == null || firstName.isEmpty() || lastName.isEmpty() || username.isEmpty() || password.isEmpty()) {
            response.setCharacterEncoding(CHARSET);
            response.setContentType("application/json");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        service.save(firstName, lastName, username, password);
        response.sendRedirect(context.getContextPath() + "/users");
    }

    @Override
    public void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Logger logger = (Logger) context.getAttribute("logger");
        UsersService service = (UsersService) context.getAttribute("usersService");
        ObjectMapper mapper = (ObjectMapper) context.getAttribute("objectMapper");

        logger.info("PUT /user");

        User user = null;
        String firstName = null;
        String lastName = null;
        String username = null;

        try {
            User mapped = mapper.readValue(request.getReader(), User.class);
            firstName = mapped.getFirstName();
            lastName = mapped.getLastName();
            username = mapped.getUsername();
            user = service.findFirstMatch(firstName, lastName, username);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        if (user == null) {
            response.setCharacterEncoding(CHARSET);
            response.setContentType("application/json");
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        service.update(user, firstName, lastName, username);
        response.sendRedirect(context.getContextPath() + "/users");
    }

    @Override
    public void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Logger logger = (Logger) context.getAttribute("logger");
        UsersService service = (UsersService) context.getAttribute("usersService");
        ObjectMapper mapper = (ObjectMapper) context.getAttribute("objectMapper");

        logger.info("DELETE /user");

        User user = null;

        try {
            User mapped = mapper.readValue(request.getReader(), User.class);
            user = service.findFirstMatch(null, null, mapped.getUsername());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        if (user == null) {
            response.setCharacterEncoding(CHARSET);
            response.setContentType("application/json");
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        service.delete(user);
        response.sendRedirect(context.getContextPath() + "/users");
    }
}
