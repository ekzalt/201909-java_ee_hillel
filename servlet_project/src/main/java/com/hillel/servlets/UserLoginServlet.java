package com.hillel.servlets;

import com.hillel.enities.User;
import com.hillel.services.JsonService;
import com.hillel.services.LoggerService;
import com.hillel.services.UsersService;

import org.apache.logging.log4j.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/user/login")
public class UserLoginServlet extends HttpServlet {
    public static final String CHARSET = "UTF-8";
    private ServletContext context;

    @Override
    public void init() {
        context = getServletContext();

        context.setAttribute("logger", LoggerService.logger);
        context.setAttribute("usersService", UsersService.service);
        context.setAttribute("objectMapper", JsonService.mapper);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Logger logger = (Logger) context.getAttribute("logger");

        logger.info("GET /user/login");
        request
                .getRequestDispatcher("/login.jsp")
                .forward(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Logger logger = (Logger) context.getAttribute("logger");
        UsersService service = (UsersService) context.getAttribute("usersService");

        logger.info("POST /user/login");

        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        if (firstName == null || lastName == null || username == null || password == null || firstName.isEmpty() || lastName.isEmpty() || username.isEmpty() || password.isEmpty()) {
            response.sendRedirect(context.getContextPath() + "/user/login");
            return;
        }

        User user = service.findFullMatch(firstName, lastName, username, password);

        if (user == null) {
            response.sendRedirect(context.getContextPath() + "/user/login");
            return;
        }

        service.login(user);
        HttpSession session = request.getSession();
        session.setAttribute("user", user);

        response.sendRedirect(context.getContextPath() + "/user/login/success");
    }
}
