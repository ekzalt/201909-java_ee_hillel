package com.hillel.servlets;

import com.hillel.enities.User;
import com.hillel.services.JsonService;
import com.hillel.services.LoggerService;
import com.hillel.services.UsersService;

import org.apache.logging.log4j.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/user/login/success")
public class UserLoginSuccessServlet extends HttpServlet {
    public static final String CHARSET = "UTF-8";
    private ServletContext context;

    @Override
    public void init() {
        context = getServletContext();

        context.setAttribute("logger", LoggerService.logger);
        context.setAttribute("usersService", UsersService.service);
        context.setAttribute("objectMapper", JsonService.mapper);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Logger logger = (Logger) context.getAttribute("logger");

        logger.info("GET /user/login/success");

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");

        if (user == null) {
            response.setCharacterEncoding(CHARSET);
            response.setContentType("application/json");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        request.setAttribute("user", user);
        request
                .getRequestDispatcher("/login-success.jsp")
                .forward(request, response);
    }
}
