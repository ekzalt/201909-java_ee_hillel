package com.hillel.enities;

import com.hillel.enums.LoggedStatus;
import lombok.Data;

@Data
public class User {
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private LoggedStatus status;

    public User () {
        // default constructor for Jackson
    }

    public User(String firstName, String lastName, String username, String password, LoggedStatus status) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.status = status;
    }
}
