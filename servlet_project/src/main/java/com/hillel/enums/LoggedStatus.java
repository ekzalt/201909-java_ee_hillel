package com.hillel.enums;

public enum LoggedStatus {
    LOGGED,
    NOT_LOGGED,
}
