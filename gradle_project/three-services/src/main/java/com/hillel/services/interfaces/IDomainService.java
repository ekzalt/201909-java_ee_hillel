package com.hillel.services.interfaces;

public interface IDomainService<T> {
    T get();
}
