package com.hillel.services;

import com.hillel.dao.UsersRepository;
import com.hillel.entities.UserEntity;
import com.hillel.services.interfaces.IDomainService;

public class UsersService implements IDomainService<UserEntity> {
    UsersRepository repository;

    public UsersService(UsersRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserEntity get() {
        return repository.find();
    }
}
