package com.hillel;

import com.hillel.dao.OrdersRepository;
import com.hillel.dao.UsersRepository;
import com.hillel.entities.OrderEntity;
import com.hillel.entities.UserEntity;
import com.hillel.services.OrdersService;
import com.hillel.services.UsersService;

public class App {
    public static void main(String[] args) {
        UsersService usersService = new UsersService(new UsersRepository());
        OrdersService ordersService = new OrdersService(new OrdersRepository());

        UserEntity user = usersService.get();
        OrderEntity order = ordersService.get();

        System.out.println(user);
        System.out.println(order);
    }
}
