package com.hillel.dao;

import com.hillel.dao.interfaces.IRepository;
import com.hillel.entities.UserEntity;

public class UsersRepository implements IRepository<UserEntity> {
    @Override
    public UserEntity find() {
        return new UserEntity(1, "User");
    }
}
