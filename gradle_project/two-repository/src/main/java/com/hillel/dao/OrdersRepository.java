package com.hillel.dao;

import com.hillel.dao.interfaces.IRepository;
import com.hillel.entities.OrderEntity;

public class OrdersRepository implements IRepository<OrderEntity> {
    @Override
    public OrderEntity find() {
        return new OrderEntity(1, "Order");
    }
}
